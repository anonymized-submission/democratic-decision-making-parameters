from math import comb

def p_badevent(P, s, n, t, denominator = None):
    if s < t:
        return 0

    numerator = sum([comb(s, i) * comb(P-s, n-i) for i in range(t, min(s, n)+1)])
    if not denominator:
        denominator = comb(P, n)

    return numerator/denominator
