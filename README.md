### Usage

First run `python calc-probs.py` to generate `probs.json` (this step takes some
time). Probabilities for all combinations of the parameters in `parameters.json`
are calculated and stored in `probs.json`.

You may change `parameters.json` to taste.
* `ss` are values of `s`, in proportion to `P`,
* `ns` are values of `n`, in proportion to `P`,
* `ts` are values of `t`, in proportion to `n`.

After generating `probs.json`, run `python plot-probs.py` to visualise the generated probabilities.

You may also tune and run `print-probs.py` to get the exact
probability of the bad event for various numbers of total users and for tunable
values of the parameters. Currently it prints the probability from `10000`
users upwards with a step of `10`, with `n = P/100`, `s = P/4` and `t = n/2`.

`badevent.py` contains the definition of the probability function.
