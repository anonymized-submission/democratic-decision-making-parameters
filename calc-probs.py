from math import comb, floor
import numpy as np
import json
import operator
from badevent import p_badevent

with open("parameters.json", "r") as f:
    # ss in proportions of P, ns in proportions of P, ts in proportions of n
    P, ss, ns, ts = operator.itemgetter("P", "ss", "ns", "ts")(json.load(f))

probs = np.zeros((len(ns), len(ss), len(ts)))
probs.fill(None)

for ni, n in enumerate(ns):
    n = floor(P*n)
    if n == 0:
        continue
    print("  going for n:", n)
    denominator = comb(P, n)

    for si, s in enumerate(ss):
        s = floor(P*s)
        if s == 0:
            continue
        print("    going for s:", s)

        for ti, t in enumerate(ts):
            t = floor(n*t)
            if t == 0:
                continue

            print("      going for t:", t)
            probs[ni][si][ti] = p_badevent(P, s, n, t, denominator)

with open("probs.json", "w") as f:
    json.dump(probs.tolist(), f)
